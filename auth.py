#Test CI integration with github
from flask import Flask, request, redirect
app = Flask(__name__)
 


@app.after_request
def do_something_whenever_a_request_has_been_handled(response):
	# we have a response to manipulate, always return one
	shutdown_server()
	return response
	
def get_full_name():
    import ctypes
    GetUserNameEx = ctypes.windll.secur32.GetUserNameExW
    NameDisplay = 4

    size = ctypes.pointer(ctypes.c_ulong(0))
    GetUserNameEx(NameDisplay, None, size)

    nameBuffer = ctypes.create_unicode_buffer(size.contents.value)
    GetUserNameEx(NameDisplay, nameBuffer, size)
    return nameBuffer.value
